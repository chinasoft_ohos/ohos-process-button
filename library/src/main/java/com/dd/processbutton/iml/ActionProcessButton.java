package com.dd.processbutton.iml;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import com.dd.library.ResourceTable;
import com.dd.processbutton.ProcessButton;
import com.dd.processbutton.utils.AnimationUtils;
import com.dd.processbutton.utils.HmsResourcesManager;

/*
 *    The MIT License (MIT)
 *
 *   Copyright (c) 2014 Danylyk Dmytro
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included in all
 *   copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *   SOFTWARE.
 */
public class ActionProcessButton extends ProcessButton {
    private static final int INVALIDATE_TIME_INTERVAL = 25;

    private ProgressBar mProgressBar;

    private Mode mMode;

    private int mColor1;
    private int mColor2;
    private int mColor3;
    private int mColor4;

    public enum Mode {
        PROGRESS, ENDLESS
    }

    public ActionProcessButton(Context context) {
        super(context);
        init(context);
    }

    public ActionProcessButton(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ActionProcessButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        mMode = Mode.ENDLESS;

        mColor1 = HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_holo_blue_bright);
        mColor2 = HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_holo_green_light);
        mColor3 = HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_holo_orange_light);
        mColor4 = HmsResourcesManager.getColorValueByResources(context, ResourceTable.Color_holo_red_light);
    }

    public void setMode(Mode mode) {
        mMode = mode;
    }

    public void setColorScheme(int color1, int color2, int color3, int color4) {
        mColor1 = color1;
        mColor2 = color2;
        mColor3 = color3;
        mColor4 = color4;
    }

    @Override
    public void invalidate() {
        super.invalidate();
    }

    @Override
    public void drawProgress(Canvas canvas) {
        if (getBackgroundElement() != getNormalDrawable()) {
            setBackground(getNormalDrawable());
        }

        switch (mMode) {
            case ENDLESS:
                drawEndlessProgress(canvas);
                break;
            case PROGRESS:
                drawLineProgress(canvas);
                break;
            default:
                break;
        }
    }

    private void drawLineProgress(Canvas canvas) {
        float scale = (float) getProgress() / (float) getMaxProgress();
        float indicatorWidth = (float) getWidth() * scale;
        double indicatorHeightPercent = 0.05; // 5%
        int bottom = (int) (getHeight() - getHeight() * indicatorHeightPercent);
        getProgressDrawable().setBounds(0, bottom, (int) indicatorWidth, getHeight());
        RectFloat rectFloat = new RectFloat(0, bottom, (int) indicatorWidth, getHeight());
        canvas.drawRect(rectFloat, getmProgressPaint());
    }

    private void drawEndlessProgress(Canvas canvas) {
        if (mProgressBar == null) {
            mProgressBar = new ProgressBar(this);
            setupProgressBarBounds();
            mProgressBar.setColorScheme(mColor1, mColor2, mColor3, mColor4);
            mProgressBar.start();
        }

        if (getProgress() > 0) {
            mProgressBar.draw(canvas, this);
        }
    }

    private void setupProgressBarBounds() {
        double indicatorHeight = getDimension(ResourceTable.Float_layer_padding);
        int bottom = (int) (getHeight() - indicatorHeight);
        mProgressBar.setBounds(0, bottom, getWidth(), getHeight());
    }

    public static class ProgressBar {

        // Default progress animation colors are grays.
        private static final int COLOR1 = 0xB3000000;
        private static final int COLOR2 = 0x80000000;
        private static final int COLOR3 = 0x4d000000;
        private static final int COLOR4 = 0x1a000000;

        // The duration of the animation cycle.
        private static final int ANIMATION_DURATION_MS = 2000;

        // The duration of the animation to clear the bar.
        private static final int FINISH_ANIMATION_DURATION_MS = 1000;

        // Interpolator for varying the speed of the animation.
        private final Paint mPaint = new Paint();
        private final RectFloat mClipRect = new RectFloat();
        private float mTriggerPercentage;
        private long mStartTime;
        private long mFinishTime;
        private boolean mRunning;

        // Colors used when rendering the animation,
        private int mColor1;
        private int mColor2;
        private int mColor3;
        private int mColor4;
        private final Component mParent;

        private final RectFloat mBounds = new RectFloat();

        private EventHandler eventHandler;

        public ProgressBar(Component parent) {
            mParent = parent;
            mColor1 = COLOR1;
            mColor2 = COLOR2;
            mColor3 = COLOR3;
            mColor4 = COLOR4;
        }

        /**
         * Set the four colors used in the progress animation. The first color will
         * also be the color of the bar that grows in response to a user swipe
         * gesture.
         *
         * @param color1 Integer representation of a color.
         * @param color2 Integer representation of a color.
         * @param color3 Integer representation of a color.
         * @param color4 Integer representation of a color.
         */
        void setColorScheme(int color1, int color2, int color3, int color4) {
            mColor1 = color1;
            mColor2 = color2;
            mColor3 = color3;
            mColor4 = color4;
        }

        /**
         * Start showing the progress animation.
         */
        void start() {
            if (!mRunning) {
                mTriggerPercentage = 0;
                mStartTime = AnimationUtils.currentAnimationTimeMillis();

                mRunning = true;
                mParent.invalidate();
            }
        }

        void draw(Canvas canvas, ActionProcessButton actionProcessButton) {
            final int width = (int) mBounds.getWidth();
            final int height = (int) mBounds.getHeight();
            final int cx = width / 2;
            final int cy = height / 2;
            boolean drawTriggerWhileFinishing = false;
            int restoreCount = canvas.save();
            canvas.clipRect(mBounds);
            if (mRunning || (mFinishTime > 0)) {
                long now = AnimationUtils.currentAnimationTimeMillis();
                long elapsed = (now - mStartTime) % ANIMATION_DURATION_MS;
                long iterations = (now - mStartTime) / ANIMATION_DURATION_MS;
                float rawProgress = elapsed / (ANIMATION_DURATION_MS / 100f);

                // If we're not running anymore, that means we're running through
                // the finish animation.
                if (!mRunning) {
                    // If the finish animation is done, don't draw anything, and
                    // don't repost.
                    if ((now - mFinishTime) >= FINISH_ANIMATION_DURATION_MS) {
                        mFinishTime = 0;
                        return;
                    }

                    // Otherwise, use a 0 opacity alpha layer to clear the animation
                    // from the inside out. This layer will prevent the circles from
                    // drawing within its bounds.
                    long finishElapsed = (now - mFinishTime) % FINISH_ANIMATION_DURATION_MS;
                    float finishProgress = finishElapsed / (FINISH_ANIMATION_DURATION_MS / 100f);
                    float pct = finishProgress / 100f;

                    // Radius of the circle is half of the screen.
                    float clearRadius = width / 2f * getInterpolation(pct);
                    mClipRect.fuse(cx - clearRadius, 0, cx + clearRadius, height);
                    canvas.saveLayerAlpha(mClipRect, 0);

                    // Only draw the trigger if there is a space in the center of
                    // this refreshing view that needs to be filled in by the
                    // trigger. If the progress view is just still animating, let it
                    // continue animating.
                    drawTriggerWhileFinishing = true;
                }

                // First fill in with the last color that would have finished drawing.
                if (iterations == 0) {
                    canvas.drawColor(mColor1, Canvas.PorterDuffMode.SRC_OVER);
                } else {
                    if (rawProgress >= 0 && rawProgress < 25) {
                        canvas.drawColor(mColor4, Canvas.PorterDuffMode.SRC_OVER);
                    } else if (rawProgress >= 25 && rawProgress < 50) {
                        canvas.drawColor(mColor1, Canvas.PorterDuffMode.SRC_OVER);
                    } else if (rawProgress >= 50 && rawProgress < 75) {
                        canvas.drawColor(mColor2, Canvas.PorterDuffMode.SRC_OVER);
                    } else {
                        canvas.drawColor(mColor3, Canvas.PorterDuffMode.SRC_OVER);
                    }
                }

                // Then draw up to 4 overlapping concentric circles of varying radii, based on how far
                // along we are in the cycle.
                // progress 0-50 draw mColor2
                // progress 25-75 draw mColor3
                // progress 50-100 draw mColor4
                // progress 75 (wrap to 25) draw mColor1
                if (rawProgress >= 0 && rawProgress <= 25) {
                    float pct = ((rawProgress + 25) * 2) / 100f;
                    drawCircle(canvas, cx, cy, mColor1, pct);
                }
                if (rawProgress >= 0 && rawProgress <= 50) {
                    float pct = (rawProgress * 2) / 100f;
                    drawCircle(canvas, cx, cy, mColor2, pct);
                }
                if (rawProgress >= 25 && rawProgress <= 75) {
                    float pct = ((rawProgress - 25) * 2) / 100f;
                    drawCircle(canvas, cx, cy, mColor3, pct);
                }
                if (rawProgress >= 50 && rawProgress <= 100) {
                    float pct = ((rawProgress - 50) * 2) / 100f;
                    drawCircle(canvas, cx, cy, mColor4, pct);
                }
                if (rawProgress >= 75 && rawProgress <= 100) {
                    float pct = ((rawProgress - 75) * 2) / 100f;
                    drawCircle(canvas, cx, cy, mColor1, pct);
                }
                if (mTriggerPercentage > 0 && drawTriggerWhileFinishing) {
                    // There is some portion of trigger to draw. Restore the canvas,
                    // then draw the trigger. Otherwise, the trigger does not appear
                    // until after the bar has finished animating and appears to
                    // just jump in at a larger width than expected.
                    canvas.restoreToCount(restoreCount);
                    restoreCount = canvas.save();
                    canvas.clipRect(mBounds);
                    drawTrigger(canvas, cx, cy);
                }

                // Keep running until we finish out the last cycle.
                startDelay(mParent);
            } else {
                // Otherwise if we're in the middle of a trigger, draw that.
                if (mTriggerPercentage > 0 && mTriggerPercentage <= 1.0) {
                    drawTrigger(canvas, cx, cy);
                }
            }
            canvas.restoreToCount(restoreCount);
        }

        private void startDelay(Component mParent) {
            if (eventHandler == null) {
                eventHandler = new EventHandler(EventRunner.getMainEventRunner());
            }
            eventHandler.postTask(new Runnable() {
                @Override
                public void run() {
                    mParent.invalidate();
                }
            }, INVALIDATE_TIME_INTERVAL);
        }

        private void drawTrigger(Canvas canvas, int cx, int cy) {
            mPaint.setColor(new Color(mColor1));
            canvas.drawCircle(cx, cy, cx * mTriggerPercentage, mPaint);
        }

        /**
         * Draws a circle centered in the view.
         *
         * @param canvas the canvas to draw on
         * @param cx     the center x coordinate
         * @param cy     the center y coordinate
         * @param color  the color to draw
         * @param pct    the percentage of the view that the circle should cover
         */
        private void drawCircle(Canvas canvas, float cx, float cy, int color, float pct) {
            mPaint.setColor(new Color(color));
            canvas.save();
            canvas.translate(cx, cy);
            float radiusScale = getInterpolation(pct);
            canvas.scale(radiusScale, radiusScale);
            canvas.drawCircle(0, 0, cx, mPaint);
            canvas.restore();
        }

        /**
         * Set the drawing bounds of this SwipeProgressBar.
         *
         * @param left left point
         * @param top top point
         * @param right right point
         * @param bottom bottom point
         */
        void setBounds(int left, int top, int right, int bottom) {
            mBounds.left = left;
            mBounds.top = top;
            mBounds.right = right;
            mBounds.bottom = bottom;
        }
    }

    /**
     * 计算插值
     *
     * @param input 输入值
     * @return 结果
     */
    public static float getInterpolation(float input) {
        return (float) (Math.cos((input + 1) * Math.PI) / 2.0f) + 0.5f;
    }
}
