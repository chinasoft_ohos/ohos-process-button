/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.processbutton.utils;

import ohos.app.Context;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.IOException;

/**
 * 资源获取管理类
 *
 * @since 2021-03-01
 */
public class HmsResourcesManager {
    private static final HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP,
            0x78002, HmsResourcesManager.class.getSimpleName());

    private HmsResourcesManager() {
    }

    /**
     * 获取float值
     *
     * @param context 上下文
     * @param resourcesId 资源id
     * @return float值
     */
    public static float getFloatValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getFloat();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getFloatValueByResources err exception = " + e);
            }
        }
        return 0;
    }

    /**
     * 获取Color值
     *
     * @param context 上下文
     * @param resourcesId 资源id
     * @return color值
     */
    public static int getColorValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getColor();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getColorValueByResources err exception = " + e);
            }
        }
        return 0;
    }

    /**
     * 获取String值
     *
     * @param context 上下文
     * @param resourcesId 资源id
     * @return String值
     */
    public static String getStringValueByResources(Context context, int resourcesId) {
        Element element = get(context, resourcesId);
        if (element != null) {
            try {
                return element.getString();
            } catch (NotExistException | WrongTypeException | IOException e) {
                HiLog.info(hiLogLabel, "getColorValueByResources err exception = " + e);
            }
        }
        return "";
    }

    private static Element get(Context context, int resourcesId) {
        Element element = null;
        try {
            element = context.getResourceManager().getElement(resourcesId);
        } catch (NotExistException | WrongTypeException | IOException e) {
            HiLog.info(hiLogLabel, "get err exception = " + e);
        }
        return element;
    }
}
