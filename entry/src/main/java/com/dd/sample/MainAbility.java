/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.sample;

import com.dd.sample.slice.GenerateButtonSlice;
import com.dd.sample.slice.StateSampleSlice;
import com.dd.sample.slice.SubmitAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * 主测试页面
 *
 * @since 2021-03-01
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        findComponentById(ResourceTable.Id_button_progress).setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = getOperation(SignInAbility.class.getName());

            intent2.setParam(SignInAbility.EXTRAS_ENDLESS_MODE, false);
            intent2.setOperation(operation);
            startAbility(intent2);
        });
        findComponentById(ResourceTable.Id_endless_progress).setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = getOperation(SignInAbility.class.getName());

            intent2.setParam(SignInAbility.EXTRAS_ENDLESS_MODE, true);
            intent2.setOperation(operation);
            startAbility(intent2);
        });
        findComponentById(ResourceTable.Id_submit_btn).setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = getOperation(SubmitAbilitySlice.class.getName());

            intent2.setParam(SignInAbility.EXTRAS_ENDLESS_MODE, true);
            intent2.setOperation(operation);
            startAbility(intent2);
        });
        findComponentById(ResourceTable.Id_generate_btn).setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = getOperation(GenerateButtonSlice.class.getName());
            intent2.setOperation(operation);
            startAbility(intent2);
        });
        findComponentById(ResourceTable.Id_state_sample).setClickedListener(component -> {
            Intent intent2 = new Intent();
            Operation operation = getOperation(StateSampleSlice.class.getName());
            intent2.setOperation(operation);
            startAbility(intent2);
        });
    }

    private Operation getOperation(String abilityName) {
        return new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
