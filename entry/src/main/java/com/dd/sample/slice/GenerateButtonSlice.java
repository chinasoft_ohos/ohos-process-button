/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dd.sample.slice;

import com.dd.processbutton.iml.GenerateProcessButton;
import com.dd.sample.ResourceTable;
import com.dd.sample.Toast;
import com.dd.sample.utils.ProgressGenerator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * 测试GenerateProcessButton
 *
 * @since 2021-03-01
 */
public class GenerateButtonSlice extends Ability {
    private GenerateProcessButton generateProcessButton;
    private ProgressGenerator progressGenerator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_generate_button_slice);
        generateProcessButton = (GenerateProcessButton) findComponentById(ResourceTable.Id_generate_process_btn);
        progressGenerator = new ProgressGenerator(new ProgressGenerator.OnCompleteListener() {
            @Override
            public void onComplete() {
                Toast.showLong(getContext(),"Loading Complete, button is disabled");
            }
        });
        generateProcessButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                progressGenerator.start(generateProcessButton);
                generateProcessButton.setEnabled(false);
            }
        });
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        generateProcessButton.setProgress(0);
        generateProcessButton.setEnabled(true);
        progressGenerator.stop();
    }
}
