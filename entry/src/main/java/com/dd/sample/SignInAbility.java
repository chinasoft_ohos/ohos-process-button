package com.dd.sample;

import com.dd.processbutton.iml.ActionProcessButton;
import com.dd.sample.utils.ProgressGenerator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;

public class SignInAbility extends Ability {
    static final String EXTRAS_ENDLESS_MODE = "EXTRAS_ENDLESS_MODE";

    private ActionProcessButton actionProcessButton;
    private TextField num;
    private TextField pass;
    private ProgressGenerator progressGenerator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_button_progress_slice);
        actionProcessButton = (ActionProcessButton) findComponentById(ResourceTable.Id_btnSignIn);
        num = (TextField) findComponentById(ResourceTable.Id_user_num);
        pass = (TextField) findComponentById(ResourceTable.Id_user_password);

        if (progressGenerator == null) {
            progressGenerator = new ProgressGenerator(new ProgressGenerator.OnCompleteListener() {
                @Override
                public void onComplete() {
                    Toast.showLong(getContext(), "Loading Complete, button is disabled");
                    actionProcessButton.setEnabled(false);
                }
            });
        }

        boolean booleanParam = intent.getBooleanParam(EXTRAS_ENDLESS_MODE, false);
        if (booleanParam) {
            actionProcessButton.setMode(ActionProcessButton.Mode.ENDLESS);
        } else {
            actionProcessButton.setMode(ActionProcessButton.Mode.PROGRESS);
        }
        actionProcessButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                num.setEnabled(false);
                pass.setEnabled(false);
                num.clearFocus();
                pass.clearFocus();
                progressGenerator.start(actionProcessButton);
                actionProcessButton.setEnabled(false);
            }
        });

    }

    @Override
    protected void onBackground() {
        super.onBackground();
    }


    @Override
    protected void onStop() {
        super.onStop();
        progressGenerator.stop();
        actionProcessButton.setProgress(0);
        actionProcessButton.setEnabled(true);
        num.setText("jondoe");
        pass.setText("123456789");
        num.setEnabled(true);
        pass.setEnabled(true);
    }

}
